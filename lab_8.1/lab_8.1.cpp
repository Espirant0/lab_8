﻿#include <fstream>
#include <iostream>


int main() {
    int n;
    std::cin >> n;
    int* mas = new int[n];
    int* first = new int[n];
    int* umn = new int[n];
    for (int i = 0; i < n; i++) {
        std::cin >> mas[i];
        umn[i] = 1;
        int x = mas[i];
        while (x > 0)
        {
            first[i] = x % 10;
            umn[i] *= x % 10;
            x /= 10;
        }

    }
    for (int i = 0; i < n - 1; i++)
        for (int j = i + 1; j < n; j++)
        {
            if (first[i] > first[j])
            {
                std::swap(mas[i], mas[j]);
                std::swap(first[i], first[j]);
                std::swap(umn[i], umn[j]);
            }
            else if (first[i] == first[j] && umn[i] > umn[j])
            {
                std::swap(mas[i], mas[j]);
                std::swap(first[i], first[j]);
                std::swap(umn[i], umn[j]);
            }
            else if (first[i] == first[j] && umn[i] == umn[j] && mas[i] > mas[j])
            {
                std::swap(mas[i], mas[j]);
                std::swap(first[i], first[j]);
                std::swap(umn[i], umn[j]);
            }


        }
    for (int i = 0; i < n; i++) 
        std::cout << mas[i] << " ";
    delete[] mas;
    delete[] umn;
    delete[] first;
}