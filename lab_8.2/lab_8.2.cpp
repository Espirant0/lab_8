﻿#include <iostream>

int main()
{
    int n, m, k;
    int min = INT_MAX;
    int sum = 0;
    std::cin >> n;
    std::cin >> m;
    int** matrix = new int* [n];
    for (int i = 0; i < n; i++)
        matrix[i] = new int[m];

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            std::cin >> matrix[i][j];

    for (int j = 0; j < n; j++) {

        for (int i = 0; i < n; i++) {
            sum = sum + matrix[i][j];
        }
        if (sum < min) {
            min = sum;
            k = j;
        }
        sum = 0;

    }
    for (int j = 0; j < m; j++)
    {
        for (int i = 0; i < n; i++)
        {
            sum = sum + matrix[i][j];
        }
        if (sum == min)
        {
            for (int t = 0; t < m; t++)
            {
                matrix[t][k] = matrix[t][k] + 3;
            }
        }
        sum = 0;
    }


    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++) {
            std::cout << matrix[i][j] << " ";
        }
        std::cout << std::endl;
    }
    for (int i = 0; i < n; i++)
        delete[] matrix[i];
    delete[] matrix;

}

